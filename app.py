# on cmd: set FLASK_APP=app.py
# flask run --host=0.0.0.0

from flask import Flask, request
from flask_cors import CORS
import schedule
import time
import json
from services import search_user, get_lastDataArduino, insert_newDefault, get_dataFromDefault, update_default

app = Flask(__name__)
CORS(app)


@app.route('/user/<username>', methods=['GET'])
def someFunction(username):
    x = search_user(username)
    return x


@app.route('/arduino/<id>', methods=['GET'])
def dataFromArduino(id):
    print('Entrou aqui..')
    result = get_lastDataArduino(id)
    return result

@app.route('/default/<id>', methods=['GET'])
def dataToDefaulT(id):
    try:
        result = get_dataFromDefault(id)
        return result
    except Exception as e:
        print('Erro API 1..', e)
        return False


@app.route('/defaults/', methods=['POST'])
def saveDataToDefaulT():
    try:
        data = json.loads(request.data)
        print('O q veio na req: ', data)
        result = get_dataFromDefault(data["id_user"])
        print('O q tem no banco: ', result)
        if result:
            rs = update_default(data['default_name'], data['l_moistureg'], data['l_moisture'],
                data['l_temperature'], data['id_user'])
            if rs:
                return {"msg": "Atualizou"}
            else:
                return {"msg": "Erro ao atualizar"}
        else:
            rs = insert_newDefault(data['default_name'], data['l_moistureg'], data['l_moisture'],
                data['l_temperature'], data['id_user'])
            if rs:
                return {"msg": "Padrão definido"}
            else:
                return {"msg": "Erro ao cadastrar um novo padrão"}
        return
    except Exception as e:
        print('Erro API 2..', e)
        return {"msg": "Erro ao atualizar!"}
