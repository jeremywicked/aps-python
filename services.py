import mysql.connector


def connect_db():
    _db = None
    try:
        _db = mysql.connector.connect(
            host='localhost', database='db_aps', user='root',  password='', port="3308")
        return _db
    except Exception as e:
        print('Erro 1..', e)
        return False

def insert_user(username, id_arduino):
    try:
        _db = connect_db()
        cur = _db.cursor()
        values = (username, id_arduino)
        sql = 'insert into users(username, id_arduino) values (%s, %s)'
        cur.execute(sql, values)
        cur.close()
        _db.commit()
    except Exception as e:
        print('Erro 2..', e)
        _db.rollback()
        return False
    return True

def search_user(username):
    rs = None
    try:
        _db = connect_db()
        cur = _db.cursor()
        sql = "select id, username, id_arduino from users where username = %s"
        cur.execute(sql, (username,))
        rows = cur.fetchall()

        result = []
        for row in rows:
            result = {
                'id': row[0],
                'username': row[1],
                'id_arduino': row[2]
            }
        print('rs', result)
        return result
    except Exception as e:
        print('Erro 3..', e)
        return False

def insert_dataToArduino(l_moistureg, l_moisture, l_temperature, id_user):
    try:
        _db = connect_db()
        cur = _db.cursor()
        sql = "insert into data_arduino (l_moistureg, l_moisture, l_temperature, id_user) values (%s, %s, %s, %s)"
        values = (l_moistureg, l_moisture, l_temperature, id_user)
        if cur.execute(sql, values):
            _db.close()
            _db.commit()
            print('deu ..')
        return 'True'
    except Exception as e:
        print('Erro 4..', e)
        _db.rollback()
        return False

def get_lastDataArduino(id):
    try:
        _db = connect_db()
        cur = _db.cursor()
        sql = "SELECT * FROM data_arduino WHERE id_user = %s ORDER BY id DESC LIMIT 1"
        cur.execute(sql, (id,))
        rows = cur.fetchall()

        result = []
        for row in rows:
            result = {
                'id': row[0],
                'l_moistureg': row[1],
                'l_moisture': row[2],
                'l_temperature': row[3],
                'id_user': row[4],
            }
        _db.close()
        _db.commit()
        print('deu ..')
        if(result):
            return result
        return False
    except Exception as e:
        print('Erro 5..', e)
        return False

def insert_newDefault(default_name, l_moistureg, l_moisture, l_temperature, id_user):
    try:
        _db = connect_db()
        cur = _db.cursor()
        sql = "insert into defaults (default_name, l_moistureg, l_moisture, l_temperature, id_user) values (%s, %s, %s, %s, %s)"
        values = (default_name, l_moistureg,
                  l_moisture, l_temperature, id_user)
        rs = cur.execute(sql, values)
        if rs is None:
            _db.close()
            _db.commit()
            return True
        else:
            return False
    except Exception as e:
        print('Erro 6..', e)
        _db.rollback()
        return False

def get_dataFromDefault(id_user):
    rs = None
    try:
        _db = connect_db()
        cur = _db.cursor()
        sql = "select id, default_name, l_moistureg, l_moisture, l_temperature, id_user from defaults where id_user = %s"
        cur.execute(sql, (id_user,))
        rows = cur.fetchall()

        result = []
        for row in rows:
            result = {
                'id': row[0],
                'default_name': row[1],
                'l_moistureg': row[2],
                'l_moisture': row[3],
                'l_temperature': row[4],
                'id_user': row[5],
            }
        _db.close()
        _db.commit()
        if result:
            return result
        else:
            return False
    except Exception as e:
        print('Erro 7..', e)
        return None

def update_default(default_name, l_moistureg, l_moisture, l_temperature, id_user):
    rs = None
    try:
        _db = connect_db()
        cur = _db.cursor()
        sql = """update defaults set default_name = %s, l_moistureg = %s, l_moisture = %s, l_temperature = %s where id_user = %s"""
        values = (default_name, l_moistureg, l_moisture, l_temperature, id_user)
        rs = cur.execute(sql, values)
        if rs is None:
            _db.commit()
            _db.close()
            print('Atualizou..')
            return True
        else:
            raise Exception
            return False
    except Exception as e:
        print('Erro 8..', e)
        return False
